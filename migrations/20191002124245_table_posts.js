
exports.up = function(knex) {
  return knex.schema.createTable('posts', table => {
    table.increments('id').primary(),
    table.string('title', 40).notNull(),
    table.string('description'),
    table.timestamp('created_at', { precision: 6 }).defaultTo(knex.fn.now(6)).notNull(),
    table.integer('user_id').unsigned(),
    table.foreign('user_id').references('users.id')
  })
};

exports.down = function(knex) {
    return knex.schema.dropTable('posts')
};
