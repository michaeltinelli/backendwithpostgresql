
exports.up = function(knex) {
    return knex.schema.createTable('roles', table => {
        table.increments('id').primary(),
        table.string('name').notNull()
    }).then(_ => {
        return knex('roles').insert([
            { name: 'Administrator' },
            { name: 'Common' },
        ])
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('roles')
};
