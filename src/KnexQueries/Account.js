
const { connection } = require('../../.env')

const knex = require('knex')({
    client: 'postgresql',
    connection
})

const table = 'users'

module.exports = {
    async queryGetUserByEmail(email) {
        return await knex(table).where({ email }).select('*').first()
        
    }
}