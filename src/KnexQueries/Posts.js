const { connection } = require('../../.env')
const knex = require('knex')({
    client: 'postgresql',
    connection
})
const table = 'posts'

module.exports = {
    async queryGetPostsById(user_id) {
        return await knex(table).where({ user_id }).select('*').orderBy('created_at', 'desc')
    },
    async queryDeletePostById(user_id, id) {
        return await knex(table).where({ user_id, id }).delete()
    },
    async queryDeleteAllPostsByUserId(user_id) {
        return await knex(table).where({ user_id, id }).delete()
    }
}
