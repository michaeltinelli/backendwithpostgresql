const rolesQueries = require('../KnexQueries/Roles')

module.exports = {
    async addRole(req, res) {
        const { name } = req.body
        const role = await rolesQueries.queryAddRole({ name })
        return res.json({ status: 200, role })
    },
    async getRoles(req, res) {
        const roles = await rolesQueries.queryGetRoles()
        return res.json({ status: 200, roles})
    },
    async getRolesOfUser(req, res) {
        const rolesUser = await rolesQueries.queryGetRolesOfUser(req.headers.user_id)
        //console.warn(rolesUser)
        res.json({ status: 200, roles: rolesUser})
    }
}