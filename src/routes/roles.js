const express = require('express')
const router = express.Router()
const rolesController = require('../controller/Roles')
const { verifyJWT } = require('../config/jwtFunctions')

router.post('/role', verifyJWT, rolesController.addRole)
router.get('/roles', rolesController.getRoles)
router.get('/roles/user', verifyJWT, rolesController.getRolesOfUser)

module.exports = router