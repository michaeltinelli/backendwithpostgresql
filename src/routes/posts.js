const express = require('express')
const router = express.Router()
const postController = require('../controller/Posts')
const { verifyJWT } = require('../config/jwtFunctions')

router.get('/posts', verifyJWT ,postController.getPostsById)

module.exports = router