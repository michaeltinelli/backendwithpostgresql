const express = require('express')
const router = express.Router()
const multer = require('multer')
const uploadConfig = require('../config/upload')
const photoImagePostController = require('../controller/ImagePost')
const uploadPhoto = multer({storage: uploadConfig.storagePost})

const { verifyJWT } = require('../config/jwtFunctions')

router.post('/imagePost', verifyJWT ,uploadPhoto.single('photo'), photoImagePostController.savePhoto)
router.delete('/imagePost', verifyJWT, photoImagePostController.deletePhoto)

module.exports = router