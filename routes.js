
const routerUsers = require('./src/routes/users')
const routerPosts = require('./src/routes/posts')
const routerPhotoProfile = require('./src/routes/photoProfile')
const routerImagePost = require('./src/routes/imagePost')
const routerAccount = require('./src/routes/account')
const routerRoles = require('./src/routes/roles')

module.exports = {
    
    routerUsers,
    routerPosts,
    routerPhotoProfile,
    routerImagePost,
    routerAccount,
    routerRoles
}