
exports.up = function(knex) {
    return knex.schema.createTable('photo_profile', table => {
        table.increments('id').primary(),
        table.string('path_photo'),
       
        table.integer('user_id').unsigned(),
        table.foreign('user_id').references('users.id')
      })
};

exports.down = function(knex) {
    return knex.schema.dropTable('photo_profile')
};
