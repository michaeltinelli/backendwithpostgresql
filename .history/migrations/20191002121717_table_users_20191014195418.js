
exports.up = function(knex) {
  return knex.schema.createTable('users', table => {
      table.increments('id').primary(),
      table.string('first_name', 20).notNull(),
      table.string('last_name', 30).notNull(),
      table.string('email').notNull().unique(),
      table.string('password').notNull()
  })
};

exports.down = function(knex) {
    return knex.schema.dropTable('users')
};
