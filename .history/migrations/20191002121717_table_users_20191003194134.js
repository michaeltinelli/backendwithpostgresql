
exports.up = function(knex) {
  return knex.schema.createTable('users', table => {
      table.increments('id').primary(),
      table.string('first_name', 20).notNull(),
      table.string('last_name', 30).notNull(),
      table.string('email').notNull().unique(),
      table.string('password').notNull()
  }).then( () => {
      return knex('users').insert([
        { first_name: 'Michael', last_name: 'Tinelli', email: 'michael@gmail.com', password: '123456' },
        { first_name: 'Maria', last_name: 'Silva', email: 'maria@gmail.com', password: '123456' },
        { first_name: 'Felipe', last_name: 'Smith', email: 'felipe@gmail.com', password: '123456' }
      ])
  })
};

exports.down = function(knex) {
    return knex.schema.dropTable('users')
};
