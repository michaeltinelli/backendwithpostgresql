
exports.up = function(knex) {
    return knex.schema.createTable('roles', table => {
        table.increments('id').primary(),
        table.string('role'),
       
        table.integer('user_id').unsigned(),
        table.foreign('user_id').references('users.id')
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('roles')
};
