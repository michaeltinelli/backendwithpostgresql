
exports.up = function(knex) {
  return knex.schema.createTable('posts', table => {
    table.increments('id').primary(),
    table.string('title', 40).notNull(),
    table.string('description'),
    table.timestamps('created_at', { precision: 6 }).defaultTo(knex.fn.now(6)).notNull(),
    table.integer('user_id').unsigned(),
    table.foreign('user_id').references('users.id')
  }).then(_ => {
      return knex('posts').insert([
        { title: 'Como eu fiquei rico!', description: 'È mentira. Continuo pobre....',
        created_at: new Date().getTime(), user_id: 1 },

        { title: 'Estou mais magro', description: null,
        created_at: new Date().getTime(), user_id: 1 },

        { title: 'Hoje tem mengão', description: 'Vai ser 1x0 com gol de Obina',
        created_at: new Date().getTime(), user_id: 2 }, 
      ])
  })
};

exports.down = function(knex) {
    return knex.schema.dropTable('posts')
};
