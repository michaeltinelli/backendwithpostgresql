
exports.up = function(knex) {
    return knex.schema.createTable('users_roles', table => {       
        table.integer('user_id').unsigned(),
        table.foreign('user_id').references('users.id'),

        table.integer('role_id').unsigned(),
        table.foreign('role_id').references('roles.id')
    }).then(_ => {
        return knex('users_roles').insert([
            { user_id: 1, role_id: 1 },
            { user_id: 2, role_id: 2 },
            { user_id: 1, role_id: 2 },
        ])
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('users_roles')
};
