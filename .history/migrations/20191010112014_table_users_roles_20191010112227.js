
exports.up = function(knex) {
    return knex.schema.createTable('users_roles', table => {       
        table.integer('user_id').unsigned(),
        table.foreign('user_id').references('users.id'),

        table.integer('role_id').unsigned(),
        table.foreign('role_id').references('roles.id')
      })
};

exports.down = function(knex) {
    return knex.schema.dropTable('users_roles')
};
