const userController = require('./src/controller/Users')
const express = require('express')
const router = express.Router()

router.get('/users', userController.getUsers)
router.post('/users', userController.addUser)

module.exports = router