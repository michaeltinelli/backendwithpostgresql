const userController = require('./src/controller/Users')
const postController = require('./src/controller/Posts')
const photoProfileController = require('./src/controller/PhotoProfile')

const express = require('express')
const router = express.Router()
const multer = require('multer')
const uploadConfig = require('./src/config/upload')


const uploadPhoto = multer({storage: uploadConfig.storagePhoto})

router.get('/users', userController.getUsers)
router.post('/users', userController.addUser)
router.get('/users/:id', userController.getUser)
router.delete('/users/:id', userController.deleteUser)

router.get('/posts', postController.getPostsById)

router.post('/photo', uploadPhoto.single('photo'), photoProfileController.savePhoto)



module.exports = router