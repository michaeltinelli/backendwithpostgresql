const multer = require('multer')
const path = require('path')
const uuid = require('uuid')
const fs = require('fs')


async function createPostsFolder() {
    const folder = await fs.mkdirSync(path.resolve(__dirname, '..', '..', 'images'))
    console.warn(folder)
    return folder
}

module.exports = {
    storage: multer.diskStorage({
        destination: createPostsFolder(),
        filename: (req, file, cb) => {
            const ext = path.extname(file.originalname)
            const name = path.basename(file.originalname, ext)
            cb(null, `${name}-${Date.now()}${ext}`)
        }
    })
}