const bcrypt = require('bcrypt')
const saltRounds = 10


module.exports = {
    async toHashPassword(password) {
        
        const salt = await bcrypt.genSaltSync(saltRounds)
        
        return await bcrypt.hashSync(password, salt)
    },
    async toComparePassword(password, pswdEncrypted) {
        bcrypt.compare(password, pswdEncrypted, (function (err, resp) {
            console.warn(resp)
            console.error(err)
        }))
    }
}