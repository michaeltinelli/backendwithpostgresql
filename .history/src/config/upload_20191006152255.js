const multer = require('multer')
const path = require('path')
const uuid = require('uuid/v1')
const fs = require('fs')
const fs_extra = require('fs-extra')


module.exports = {
    storagePhoto: 
        multer.diskStorage({
            destination: (req, file, cb) => {
                const { user_id } = req.headers
                
                const dirUser = path.resolve(__dirname, '..', '..', 'images', `${user_id}`)
                fs_extra.ensureDirSync(dirUser)

                console.warn(dirUser)
                const dirUserPhoto = path.resolve(__dirname, '..', '..', 'images', `${dirUser}`, 
                'photoProfile')

                cb(null, fs_extra.ensureDirSync(dirUserPhoto))

            },
            filename: (req, file, cb) => {
                const ext = path.extname(file.originalname)
                //const name = path.basename(file.originalname, ext)
                cb(null, `${uuid()}-${Date.now()}${ext}`)
            }
    })
}