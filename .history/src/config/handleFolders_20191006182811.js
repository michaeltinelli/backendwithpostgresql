
const fs_extra = require('fs-extra')
const path = require('path')

module.exports = {
    createFolders(user_id) {
        const dirUser = path.resolve(__dirname, '..', '..', 'images', `${user_id}`)
        fs_extra.ensureDirSync(dirUser)

        const photoUserDir =  path.resolve(__dirname, '..', '..', 'images', `${dirUser}`, 
        'photoProfile')
        fs_extra.ensureDirSync(photoUserDir)
    }
}