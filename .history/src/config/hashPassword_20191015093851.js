const bcrypt = require('bcrypt')
const crypto = require('crypto'
)
module.exports = {
    async toHashPassword(password) {
        const salt = await bcrypt.genSalt()
        return await bcrypt.hashSync(password, salt).toString()
    },
    async toComparePassword(password, pswdEncrypted) {
        //console.warn(password, pswdEncrypted)
        return await bcrypt.compare(password, pswdEncrypted)
    },
    async toCryptPassword(password) {
        const salt = await crypto.randomBytes(16).toString('hex')
        return await crypto.pbkdf2Sync(password, salt,  
            1000, 64, `sha512`).toString(`hex`);
    }
}