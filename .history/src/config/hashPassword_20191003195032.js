const bcrypt = require('bcrypt-nodejs')
const saltRounds = 10


module.exports = {
    async toHashPassword(password) {
        
        const salt = await bcrypt.genSaltSync(saltRounds)
        
        return await bcrypt.hashSync(password, salt)
    }
}