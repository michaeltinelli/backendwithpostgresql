

module.exports = function messageError(error) {
    const messageDeveloper = error
    const messageUser = ''

    if(error.code === '23503') {
        messageUser = 'Requisição inválida.'
    }

    return {
        messageDeveloper,
        messageUser
    }
}