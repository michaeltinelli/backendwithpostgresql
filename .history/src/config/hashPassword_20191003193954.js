const bcrypt = require('bcrypt-nodejs')
const saltRounds = 10


module.exports = {
    async toHashPassword(password) {
        console.log(password)
        const salt = await bcrypt.genSaltSync(saltRounds)
        console.log(salt)
        return await bcrypt.hashSync(password, salt)
    }
}