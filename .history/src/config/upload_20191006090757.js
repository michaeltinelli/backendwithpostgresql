const multer = require('multer')
const path = require('path')
const uuid = require('uuid/v1')
const fs = require('fs')
const fs_extra = require('fs-extra')


module.exports = {

    async createFolder(id, type) {
        const dir = path.resolve(__dirname, '..', '..', 'images', id, 
        type === 'photo' ? 'photo_profile' : 'posts')

        try {
            await fs_extra.ensureDir(dir)
            console.log('funcionou')
        } catch (error) {
            console.error(error)
        }
        
    },

    storage: function saveFileStogare(type) {
        multer.diskStorage({
            destination: (req, file, cb) => {
                const { user_id } = req.headers
                cb(error, createFolder(user_id, type))
            },
            filename: (req, file, cb) => {
                const ext = path.extname(file.originalname)
                //const name = path.basename(file.originalname, ext)
                cb(null, `${uuid()}-${Date.now()}${ext}`)
            }
        })

    }
}