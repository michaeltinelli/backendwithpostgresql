const multer = require('multer')
const path = require('path')
const uuid = require('uuid/v1')
const fs = require('fs')
const fs_extra = require('fs-extra')

async function createFolder(id, type) {
    const dir = path.resolve(__dirname, '..', '..', 'images', id, 
    type === 'photo' ? 'photo_profile' : 'posts')

    try {
        console.log('funcionou')
        return await fs_extra.ensureDirSync(dir)
    } catch (error) {
        console.error(error)
        return error
    }
    
}

module.exports = {


    storage: 
        multer.diskStorage({
            destination: (req, file, cb) => {
                const { user_id, type } = req.headers
                

                console.warn(type)

                const dirUser = path.resolve(__dirname, '..', '..', 'images', `${user_id}`)
                fs_extra.ensureDirSync(dirUser)
                const dirUserPhotoOrProfile = path.resolve(__dirname, '..', '..', 'images', `${dirUser}`, 
                type === 'photo' ? 'photoProfile' : 'imagesPosts')

                cb(null, fs_extra.ensureDirSync(dirUserPhotoOrProfile))

            },
            filename: (req, file, cb) => {
                const ext = path.extname(file.originalname)
                //const name = path.basename(file.originalname, ext)
                cb(null, `${uuid()}-${Date.now()}${ext}`)
            }
        })
}