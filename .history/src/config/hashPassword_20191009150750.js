const bcrypt = require('bcrypt')
const saltRounds = 10


module.exports = {
    async toHashPassword(password) {
        
        const salt = await bcrypt.genSaltSync(saltRounds)
        
        return await bcrypt.hashSync(password, salt)
    },
    async toComparePassword(passsowrd, pswdEncrypted) {
        return bcrypt.compareSync(passsowrd, pswdEncrypted)
    }
}