const multer = require('multer')
const path = require('path')
const uuid = require('uuid/v1')

const fs_extra = require('fs-extra')


module.exports = {
    storagePhoto: 
        multer.diskStorage({
            destination: (req, file, cb) => {
                const { user_id } = req.headers
                
                const dirUser = path.resolve(__dirname, '..', '..', 'images', `${user_id}`, 'photoProfile')
                cb(null, dirUser)

            },
            filename: (req, file, cb) => {
                const ext = path.extname(file.originalname)
                //const name = path.basename(file.originalname, ext)
                cb(null, `${uuid()}-${Date.now()}${ext}`)
            }
    }),
    storagePost: 
        multer.diskStorage({
            destination: (req, file, cb) => {
                const { user_id } = req.headers
                const pathPost = path.resolve(__dirname, '..', '..', 'images', `${user_id}`,
                'imagesPosts')
                
                cb(null, fs_extra.ensureDirSync(pathPost))

            },
            filename: (req, file, cb) => {
                
                const ext = path.extname(file.originalname)
                console.warn(file)

                cb(null, `${uuid()}${ext}`)
            }
    })
}