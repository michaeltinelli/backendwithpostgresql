const bcrypt = require('bcrypt')

module.exports = {
    async toHashPassword(password) {
        
        const salt = await bcrypt.genSalt()
        
        return await bcrypt.hash(password, salt)
    },
    async toComparePassword(password, pswdEncrypted) {
        //console.warn(password, pswdEncrypted)
        return await bcrypt.compareSync(password, pswdEncrypted.toString())
    }
}