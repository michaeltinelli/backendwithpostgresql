const bcrypt = require('bcrypt')



module.exports = {
    async toHashPassword(password) {
        
        const salt = await bcrypt.genSaltSync()
        
        return await bcrypt.hash(password, salt)
    },
    async toComparePassword(password, pswdEncrypted) {
        return await bcrypt.compare(password, pswdEncrypted)
    }
}