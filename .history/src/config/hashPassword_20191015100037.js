const bcrypt = require('bcrypt')

module.exports = {
    async toHashPassword(password) {
        const salt = await bcrypt.genSalt()
        return await bcrypt.hashSync(password, salt).toString()
    },
    async toComparePassword(password, pswdEncrypted) {
        //console.warn(password, pswdEncrypted)
        return await bcrypt.compare(password, pswdEncrypted)
    }
}