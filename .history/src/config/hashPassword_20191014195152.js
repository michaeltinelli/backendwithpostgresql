const bcrypt = require('bcrypt')



module.exports = {
    async toHashPassword(password) {
        
        const salt = await bcrypt.genSaltSync()
        
        return await bcrypt.hash(password, salt)
    },
    async toComparePassword(password, pswdEncrypted) {
        //console.warn(password, pswdEncrypted)
        bcrypt.compare(password.toString(), pswdEncrypted.toString(), (err, isMatch) => {
            console.error(err, isMatch)
        })
    }
}