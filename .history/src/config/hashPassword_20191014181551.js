const bcrypt = require('bcrypt')
const saltRounds = 10


module.exports = {
    async toHashPassword(password) {
        
        const salt = await bcrypt.genSaltSync(saltRounds)
        
        return await bcrypt.hashSync(password, salt)
    },
    toComparePassword(password, pswdEncrypted) {
        return bcrypt.compareSync(password, pswdEncrypted.toString())
    }
}