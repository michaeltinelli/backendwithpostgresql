const bcrypt = require('bcrypt')

module.exports = {
    async toHashPassword(password) {
        
        return bcrypt.genSalt(10, (async (err, salt) => {
            if(err) throw err
            return await bcrypt.hash(password, salt)
        }))
    },
    async toComparePassword(password, pswdEncrypted) {
        //console.warn(password, pswdEncrypted)
        return await bcrypt.compare(password, pswdEncrypted.toString())
    }
}