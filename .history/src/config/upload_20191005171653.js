const multer = require('multer')
const path = require('path')
const uuid = require('uuid/v1')
const fs = require('fs')


function createPostsFolder(user_id) {
    return fs.mkdirSync(path.resolve(__dirname, '..', '..', 'images', user_id))
}

module.exports = {
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            const { user_id } = req.headers
            cb(error, createPostsFolder(user_id))
        },
        filename: (req, file, cb) => {
            const ext = path.extname(file.originalname)
            //const name = path.basename(file.originalname, ext)
            cb(null, `${uuid()}-${Date.now()}${ext}`)
        }
    })
}