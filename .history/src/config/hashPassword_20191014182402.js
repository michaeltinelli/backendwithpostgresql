const bcrypt = require('bcrypt')



module.exports = {
    async toHashPassword(password) {
        
        const salt = await bcrypt.genSaltSync()
        
        return await bcrypt.hashSync(password, salt)
    },
    toComparePassword(password, pswdEncrypted) {
        return bcrypt.compareSync(password, pswdEncrypted)
    }
}