const jwt = require('jsonwebtoken')
const { secret } = require('../../.env')

module.exports = {
    async verifyJWT(req, res, next) {
        const token = req.headers['authorization'];
        console.warn(token)
        if(!token) return res.status(401).json({ status: 401, message: "Acesso negado."})
        
        jwt.verify(token, secret, function(err, decoded) {
            if(err) return res.status(401).json({ status: 401, message: "Acesso negado."})
            //console.warn(decoded)
            const user_id = decoded.id
            res.setHeader('user_id', user_id)
            next()
        })
    }
}