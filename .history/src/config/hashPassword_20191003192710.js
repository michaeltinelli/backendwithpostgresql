const bcrypt = require('bcrypt-nodejs')
const saltRounds = 10


module.exports = {
    toHashPassword(password) {
        const salt = bcrypt.genSaltSync(saltRounds)
        return bcrypt.hashSync(password, salt)
    }
}