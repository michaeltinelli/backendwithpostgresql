const bcrypt = require('bcrypt')

module.exports = {
    async toHashPassword(password) {
        
        const salt = await bcrypt.genSalt(10)
        
        return await bcrypt.hash(password, salt)
    },
    async toComparePassword(password, pswdEncrypted) {
        //console.warn(password, pswdEncrypted)
        bcrypt.compare(password, pswdEncrypted, (err, isMatch) => {
            console.error(err, isMatch)
        })
    }
}