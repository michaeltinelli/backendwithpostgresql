const multer = require('multer')
const path = require('path')
const uuid = require('uuid/v1')
const fs = require('fs')
const fs_extra = require('fs-extra')

async function createFolder(id, type) {
    const dir = path.resolve(__dirname, '..', '..', 'images', id, 
    type === 'photo' ? 'photo_profile' : 'posts')

    try {
        console.log('funcionou')
        return await fs_extra.ensureDir(dir)
    } catch (error) {
        console.error(error)
        return error
    }
    
}

module.exports = {


    storage: 
        multer.diskStorage({
            destination: (req, file, cb) => {
                const { user_id } = req.headers
                const { type } = req.body

                cb(null, createFolder(user_id, type))
            },
            filename: (req, file, cb) => {
                const ext = path.extname(file.originalname)
                //const name = path.basename(file.originalname, ext)
                cb(null, `${uuid()}-${Date.now()}${ext}`)
            }
        })
}