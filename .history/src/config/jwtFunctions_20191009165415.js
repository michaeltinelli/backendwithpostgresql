const jwt = require('jsonwebtoken')
const { secret } = require('../../.env')

module.exports = {
    async verifyJWT(req, res, next) {
        const token = req.headers['authorization'];
        
        if(!token) return res.status(401).json({ status: 401, message: "Acesso expirado."})
        
        jwt.verify(token, secret, function(err, decoded) {
            if(err)return res.status(500).json({ status: 500, message: "Acesso negado."})
            console.warn(decoded)
        })
    }
}