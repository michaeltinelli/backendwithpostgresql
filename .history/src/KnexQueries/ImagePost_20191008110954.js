const { connection } = require('../../.env')
const knex = require('knex')({
    client: 'postgresql',
    connection
})
const table = 'photo_post'

module.exports = {
    async querySavePhoto(photo) {
        const [ id ] = await knex(table).insert({...photo}).returning('id')
        return this.queryGetPhoto(id)
    },
    async queryGetPhoto(id) {
        return await knex(table).where({ id }).select('*')
    },
    async queryDeletePhoto(user_id) {
        return await knex(table).where({ user_id }).delete()
    }
}