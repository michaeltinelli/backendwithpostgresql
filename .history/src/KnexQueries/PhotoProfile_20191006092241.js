const { connection } = require('../../.env')
const knex = require('knex')({
    client: 'postgresql',
    connection
})
const table = 'photo_profile'

module.exports = {
    async savePhotoById(photo) {
        return await knex(table).where({ user_id: photo.user_id }).update({...photo}).select('*')
    },
    
}
