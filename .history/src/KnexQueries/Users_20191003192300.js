const { connection } = require('../../.env')

const knex = require('knex')({
    client: 'postgresql',
    connection
})

const table = 'users'

module.exports = {
    queryGetUsers() {
        return knex(table).select('*')
    },
    queryInsertUser(user) {
        return knex(table).insert({...user})
    }
}