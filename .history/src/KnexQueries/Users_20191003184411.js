const knex = require('knex')
const { connection } = require('../../.env')

knex({
    client: 'postgres',
    connection
})

module.exports = {
    queryGetUsers() {
        return knex('users').select('*')
    }
}