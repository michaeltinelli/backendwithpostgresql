const { connection } = require('../../.env')
const { toHashPassword } = require('../../src/config/hashPassword')
const table = 'users'

const knex = require('knex')({
    client: 'postgresql',
    connection
})


module.exports = {
    queryGetUsers() {
        return knex(table).select('*')
    },
    async queryInsertUser(user) {
        const passwordHashed = await toHashPassword(user.password)
        user.password = passwordHashed
        
        return await knex(table).insert({...user}).select('*')
    },
    async queryGetUser(id) {
        return await knex(table).where({ id }).select('*')
    },
    async queryDeleteUser(id) {
        await knex(table).where({ id }).delete().then(resp => {
            console.warn(resp)
            return resp
        }).catch(err => {
            console.log(err)
        })
        
    },
}