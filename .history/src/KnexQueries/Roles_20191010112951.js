const { connection } = require('../../.env')
const table = 'roles'

const knex = require('knex')({
    client: 'postgresql',
    connection
})

module.exports = {
    async getRoles() {
        return await knex(table).select('*')
    },
    async getRoleById(id) {
        return await knex(table).where({ id }).select('*')
    },
}