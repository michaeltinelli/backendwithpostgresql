
const { connection } = require('../../.env')

const knex = require('knex')({
    client: 'postgresql',
    connection
})

const table = 'users'

module.exports = {
    async queryGetUserByEmail(email) {
        const resp = await knex(table).where({ email }).select('*')
        return resp[0]
    }
}