const { connection } = require('../../.env')
const { toHashPassword } = require('../../src/config/hashPassword')

export const knex = require('knex')({
    client: 'postgresql',
    connection
})

const table = 'users'

module.exports = {
    queryGetUsers() {
        return knex(table).select('*')
    },
    async queryInsertUser(user) {
        const passwordHashed = await toHashPassword(user.password)
        user.password = passwordHashed
        
        const id = await knex(table).insert({...user}).returning('id')

        return 
    },
    async queryGetUser(id) {
        return await knex(table).where({ id }).select('*')
    },
    async queryDeleteUser(id) {
        await knex(table).where({ id }).delete().then(resp => {
            console.warn(resp)
            return resp
        }).catch(err => {
            console.log(err)
        })
        
    },
}