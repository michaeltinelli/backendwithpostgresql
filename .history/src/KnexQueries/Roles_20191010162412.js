const { connection } = require('../../.env')
const table = 'roles'

const knex = require('knex')({
    client: 'postgresql',
    connection
})

module.exports = {
    async queryGetRoles() {
        return await knex(table).select('*')
    },
    async queryGetRoleById(id) {
        return await knex(table).where({ id }).select('*')
    },
    async queryAddRole(role) {
        const [ id ] = await knex(table).insert({ ...role }).returning('id')
        return await this.getRoleById(id)
    },
    async queryGetRolesOfUser(id) {
        return await knex.select('roles.name').from('roles')
        .join('users_roles', 'roles.id', 'users_roles.roles_id')
        
    }
}