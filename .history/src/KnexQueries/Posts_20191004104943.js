const { knex } = require('./Users')
const table = 'posts'

module.exports = {
    async getPostsById(user_id) {
        return await knex(table).where({ user_id }).select('*')
    },
}
