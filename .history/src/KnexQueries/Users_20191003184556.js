const { connection } = require('../../.env')

const knex = require('knex')({
    client: 'postgresql',
    connection
})

module.exports = {
    queryGetUsers() {
        return knex('users').select('*')
    }
}