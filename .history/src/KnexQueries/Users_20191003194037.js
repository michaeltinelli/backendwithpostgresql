const { connection } = require('../../.env')
const { toHashPassword } = require('../../src/config/hashPassword')

const knex = require('knex')({
    client: 'postgresql',
    connection
})

const table = 'users'

module.exports = {
    queryGetUsers() {
        return knex(table).select('*')
    },
    async queryInsertUser(user) {
        const passwordHashed = await toHashPassword(user.password)
        user.password = passwordHashed
        console.log(user)
        return knex(table).insert({...user})
    }
}