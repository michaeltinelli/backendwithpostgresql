const { connection } = require('../../.env')
const table = 'roles'

const knex = require('knex')({
    client: 'postgresql',
    connection
})

module.exports = {
    async getRoles() {
        return await knex(table).select('*')
    },
    async getRoleById(id) {
        return await knex(table).where({ id }).select('*')
    },
    async addRole(role) {
        const [ id ] = await knex(table).insert({ ...role }).returning('id')
        return await this.getRoleById(id)
    },
}