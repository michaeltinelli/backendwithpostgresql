const knex = require('knex')
const { connection } = require('../../.env')

knex({
    client: 'pg',
    connection
})

module.exports = {
    queryGetUsers() {
        return knex('users').select('*')
    }
}