const knex = require('knex')

module.exports = {
    queryGetUsers() {
        return knex('users').select('*')
    }
}