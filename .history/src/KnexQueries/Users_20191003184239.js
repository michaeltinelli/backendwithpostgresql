const knex = require('knex')
const { connection } = require('../../.env')

knex({
    connection
})

module.exports = {
    queryGetUsers() {
        return knex('users').select('*')
    }
}