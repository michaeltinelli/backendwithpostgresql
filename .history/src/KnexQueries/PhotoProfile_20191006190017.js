const { connection } = require('../../.env')
const knex = require('knex')({
    client: 'postgresql',
    connection
})
const table = 'photo_profile'

module.exports = {
    async querySavePhoto(photo) {
        return await knex(table).insert({...photo}).returning('id')
    },
    async queryGetPhoto(id) {
        return await knex(table).where({ id }).select('*')
    }
    
}
