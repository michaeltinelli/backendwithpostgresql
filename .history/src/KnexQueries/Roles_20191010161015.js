const { connection } = require('../../.env')
const table = 'roles'

const knex = require('knex')({
    client: 'postgresql',
    connection
})

module.exports = {
    async getRoles() {
        return await knex(table).select('*')
    },
    async getRoleById(id) {
        return await knex(table).where({ id }).select('*')
    },
    async addRole(role) {
        const [ id ] = await knex(table).insert({ ...role }).returning('id')
        return await this.getRoleById(id)
    },
    async getRolesOfUser(id) {
        return await knex.select('roles.name').from('roles')
        .join('users', id, 'users.id')
        .join('users_roles', 'roles.id', 'users_roles.roles_id')
    }
}