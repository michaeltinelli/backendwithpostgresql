const queryImagePost = require('../KnexQueries/ImagePost')
const path = require('path')
const fs_extra = require('fs-extra')

module.exports = {
    async savePhoto(req, res) {
       
        const { user_id } = req.headers
        //const { path, destination } = req.file

        console.warn(req.file)
        //const resp = await queryImagePost.querySavePhoto({ user_id, path, destination })
       
       // return res.json(resp[0])
    },
    async deletePhoto(req, res) {
        
        const { user_id, id } = req.headers
        await fs_extra.unlinkSync(req.headers.path)
        await queryImagePost.queryDeletePhoto(user_id, id)
        
        return res.status(200).json({ message: 'Foto removida.' })
    }
}