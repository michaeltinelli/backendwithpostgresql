const jwt = require('jsonwebtoken')
const accountQueries = require('../KnexQueries/Account')
const usersQueries = require('../KnexQueries/Users')
const { toComparePassword, toHashPassword } = require('../config/hashPassword')
const { secret } = require('../../.env')
const configFolders = require('../config/handleFolders')

module.exports = {
    async login(req, res) {
        const { email, password } = req.body
        const user = await accountQueries.queryGetUserByEmail(email)
       
        if(!user) return res.status(400).send({  message: 'Email não encontrado.'})

        const isCorrect = await toComparePassword(password, user.password)
        console.log(isCorrect)

        if(isCorrect) {

            const token = jwt.sign(user, secret, { expiresIn: 60 })
            const user_id = user.id
            res.setHeader('user_id', user_id)

            return res.status(200).json({ status: 200, token })
        } 
        else {
            return res.status(400).json({  message: 'Email/Senha incorreto(s).'})
        }
    },
    async signUp(req, res) {
        const { email, password, first_name, last_name, } = req.body
        const verifyEmailIfExists = await accountQueries.queryGetUserByEmail(email)

        if(verifyEmailIfExists) { 
            return res.status(400)
            .json({ status: 400, message: 'Email já cadastrado.'})
        }
        else {
            const passwordHashed = await toHashPassword(password)

            const obj = {
                email,
                first_name,
                last_name,
                password: passwordHashed,
            }

           const newUser = await usersQueries.queryInsertUser({ ...obj })
           
           if(newUser) {
               configFolders.createFolders(newUser.id)
               const token = jwt.sign(newUser, secret, { expiresIn: 60 })
               res.setHeader('user_id', newUser.id)
    
               return res.json({ user: newUser, token})
           } else {
              return res.status(500).json({status: 500, message: 'Erro no servidor. Tente novamente.' })
           }
        }
    }
}