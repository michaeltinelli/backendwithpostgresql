const userQueries = require('../KnexQueries/Users')
const configFolders = require('../config/handleFolders')

let code = 200

module.exports = {
    async getUsers(req, res) {
        
        const resp = await userQueries.queryGetUsers()
        return res.json(resp)
    },
    async addUser(req, res) {
        const resp = await userQueries.queryInsertUser(req.body)
        configFolders.createFolders(resp[0].id)
        
        return res.json(resp[0])
    },
    async getUser(req, res) {
        
        const resp = await userQueries.queryGetUser(req.params.id)

        if(resp.length == 0) {
            code = 404
            return res.status(code).json({ message: 'Usuário não encontrado.' })
        } else {
            return res.status(code).json({ ...resp[0] })
        }
    },
    async deleteUser(req, res) {
        const resp = await userQueries.queryDeleteUser(req.params.id)
        configFolders.deleteUserFolders(req.params.id)
        return res.json({ message: resp == 1 ? 'Usuário removido.' : 'Operação inválida.' })
    }
}