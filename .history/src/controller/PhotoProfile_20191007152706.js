const queryPhotoProfile = require('../KnexQueries/PhotoProfile')
const path = require('path')
const fs_extra = require('fs-extra')

module.exports = {
    async savePhoto(req, res) {
        //const { path } = req.file
        const { user_id } = req.headers

        const pathPhoto = path.resolve('..', '..', 'images', `${user_id}`, 'PhotoProfile')

        //console.warn(req.file)
        await this.deletePhoto(req, res)
        const resp = await queryPhotoProfile.querySavePhoto({ user_id, path: pathPhoto })
        //console.log(resp)
        return res.json(resp[0])
    },
    async deletePhoto(req, res) {
        
        const { user_id } = req.headers

        const dirPhoto = path.resolve('..', '..', 'images', `${user_id}`, 'PhotoProfile')
        await fs_extra.ensureFileSync(dirPhoto)
        await fs_extra.unlinkSync(req.body.path)
    }
}