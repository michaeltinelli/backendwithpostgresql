const jwt = require('jwt-simple')
const accountQueries = require('../KnexQueries/Account')
const { toComparePassword } = require('../config/hashPassword')
const { secret } = require('../../.env')

module.exports = {
    async login(req, res) {
        const { email, password } = req.body
        const user = await accountQueries.queryGetUserByEmail(email)
        console.warn(password, user)
        const isCorrect = toComparePassword(password, user.password)
        console.log(isCorrect)

        if(!isCorrect) return res.status(400).json({ status: 400, message: 'Senha incorreta.'})
        else {
            const token = jwt.encode(user, secret)
            console.warn(token)
        }

    }
}