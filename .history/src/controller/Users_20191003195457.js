const userQueries = require('../KnexQueries/Users')

module.exports = {
    async getUsers(req, res) {
        
        const resp = await userQueries.queryGetUsers()
        return res.json(resp)
    },
    async addUser(req, res) {
        const resp = await userQueries.queryInsertUser(req.body)
        return res.json(resp)
    },
    async getUser(req, res) {
        const resp = await userQueries.queryGetUser(req.body.id)
        return res.json(resp)
    }
}