const rolesQueries = require('../KnexQueries/Roles')

module.exports = {
    async addRole(req, res) {
        const { name } = req.body
        const role = await rolesQueries.addRole({ name })
        return res.json({ status: 200, role })
    },
    async getRoles(req, res) {
        const roles = await rolesQueries.getRoles()
        return res.json({ status: 200, roles})
    }
}