const jwt = require('jsonwebtoken')
const accountQueries = require('../KnexQueries/Account')
const { toComparePassword } = require('../config/hashPassword')
const { secret } = require('../../.env')

module.exports = {
    async login(req, res) {
        const { email, password } = req.body
        const user = await accountQueries.queryGetUserByEmail(email)

        //console.warn(password, user)
        const isCorrect = await toComparePassword(password, user.password)
        console.log(isCorrect)

        if(isCorrect) {
            const token = jwt.sign(user, secret)
            
            console.warn(token)
    
            return res.json({ status: 200, token })
        } 
        else {
            return res.status(400).json({ status: 400, message: 'Senha incorreta.'})
        }

    }
}