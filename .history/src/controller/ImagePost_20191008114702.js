const queryImagePost = require('../KnexQueries/ImagePost')
const path = require('path')
const fs_extra = require('fs-extra')

module.exports = {
    async savePhoto(req, res) {
       
        const { user_id } = req.headers

        console.warn(req.file)
        //const resp = await queryImagePost.querySavePhoto({ user_id, path: req.file.path })
       
        //return res.json(resp[0])
    },
    async deletePhoto(req, res) {
        
        const { user_id } = req.headers
        await fs_extra.unlinkSync(req.headers.path)
        await queryImagePost.queryDeletePhoto(user_id)
        
        return res.status(200).json({ message: 'Foto removida.' })
    }
}