const userQueries = require('../KnexQueries/Users')
const rolesQueries = require('../KnexQueries/Roles')
const configFolders = require('../config/handleFolders')

let status = 200

module.exports = {
    async getUsers(req, res) {
        
        const resp = await userQueries.queryGetUsers()
        return res.json({ status, users: resp})
    },
    async addUser(req, res) {
        const resp = await userQueries.queryInsertUser(req.body)
        configFolders.createFolders(resp.id)
        

        return res.json(resp)
    },
    async getUser(req, res) {

        const { id } = req.params

        const resp = await userQueries.queryGetUser(id)
        const respRole = await rolesQueries.queryGetRolesOfUser(id)

        if(resp.length == 0) {
            status = 404
            return res.status(status).json({ status, message: 'Usuário não encontrado.' })
        } else {
            return res.status(status).json({ status , user: resp[0], roles: respRole })
        }
    },
    async deleteUser(req, res) {
        const resp = await userQueries.queryDeleteUser(req.params.id)
        configFolders.deleteUserFolders(req.params.id)
        return res.json({ status, message: resp == 1 ? 'Usuário removido.' : 'Operação inválida.' })
    }
}