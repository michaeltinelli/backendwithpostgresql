const postsQueries = require('../KnexQueries/Posts')

module.exports = {
    async getPostsById(req, res) {

        const user_id = req.headers.user_id
        const posts = await postsQueries.getPostsById(user_id)

        const HttpStatus = res.status(200)
        return res.json({ HttpStatus , ...posts })
    },
    async deletePostById(req, res) {

    }
}