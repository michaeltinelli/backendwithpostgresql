const userQueries = require('../KnexQueries/Users')

module.exports = {
    async getUsers(req, res) {
        
        const resp = await userQueries.queryGetUsers()
        return res.json(resp)
    }
}