const queryPhotoProfile = require('../KnexQueries/PhotoProfile')

module.exports = {
    async savePhoto(req, res) {
        const { path } = req.file
        const { user_id } = req.headers

        //console.warn(req.file)
        const resp = await queryPhotoProfile.querySavePhoto({user_id, path,})
        console.log(resp)
    }
}