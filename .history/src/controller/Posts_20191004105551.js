const postsQueries = require('../KnexQueries/Posts')
const http = require('http')

module.exports = {
    async getPostsById(req, res) {
        const user_id = req.params.id
        const posts = await postsQueries.getPostsById(user_id)

        return res.json({ HttpStatus: http.STATUS_CODES(200), posts })
    },
}