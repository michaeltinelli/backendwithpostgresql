const userQueries = require('../KnexQueries/Users')
const configFolders = require('../config/handleFolders')

module.exports = {
    async getUsers(req, res) {
        
        const resp = await userQueries.queryGetUsers()
        return res.json(resp)
    },
    async addUser(req, res) {
        const resp = await userQueries.queryInsertUser(req.body)
        configFolders.createFolders(resp[0].id)
        
        return res.json(resp[0])
    },
    async getUser(req, res) {
        const resp = await userQueries.queryGetUser(req.params.id)
        return res.json(resp[0])
    },
    async deleteUser(req, res) {
        const resp = await userQueries.queryDeleteUser(req.params.id)
        configFolders.deleteUserFolders(req.params.id)
        return res.json(resp)
    }
}