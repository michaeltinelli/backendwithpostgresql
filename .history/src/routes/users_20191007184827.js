const express = require('express')
const router = express.Router()
const userController = require('../controller/Users')

router.get('/users', userController.getUsers)
router.post('/users', userController.addUser)
router.get('/users/:id', userController.getUser)
router.delete('/users/:id', userController.deleteUser)

module.exports = router
