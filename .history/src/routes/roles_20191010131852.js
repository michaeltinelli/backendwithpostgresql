const express = require('express')
const router = express.Router()
const rolesController = require('../controller/Roles')

router.post('/role', rolesController.addRole)
router.get('/roles', rolesController.getRoles)

module.exports = router