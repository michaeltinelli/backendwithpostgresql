const express = require('express')
const router = express.Router()
const userController = require('../controller/Users')
const { verifyJWT } = require('../config/jwtFunctions')

router.get('/users', verifyJWT, userController.getUsers)
router.post('/users', verifyJWT, userController.addUser)
router.get('/users/:id', verifyJWT, userController.getUser)
router.delete('/users/:id', verifyJWT, userController.deleteUser)

module.exports = router
