const express = require('express')
const router = express.Router()
const postController = require('../controller/Posts')


router.get('/posts', postController.getPostsById)

module.exports = router