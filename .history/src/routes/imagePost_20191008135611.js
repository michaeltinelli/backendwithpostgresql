const express = require('express')
const router = express.Router()
const multer = require('multer')
const uploadConfig = require('../config/upload')
const photoImagePostController = require('../controller/ImagePost')
const uploadPhoto = multer({storage: uploadConfig.storagePost})


router.post('/imagePost', uploadPhoto.array('photo'), photoImagePostController.savePhoto)
router.delete('/imagePost', photoImagePostController.deletePhoto)

module.exports = router