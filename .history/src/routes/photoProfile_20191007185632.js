const express = require('express')
const router = express.Router()
const multer = require('multer')
const uploadConfig = require('../config/upload')
const photoProfileController = require('../controller/PhotoProfile')
const uploadPhoto = multer({storage: uploadConfig.storagePhoto})


router.post('/photo', uploadPhoto.single('photo'), photoProfileController.savePhoto)
router.delete('/photo', photoProfileController.deletePhoto)

module.exports = router