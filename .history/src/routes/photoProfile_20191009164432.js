const express = require('express')
const router = express.Router()
const multer = require('multer')
const uploadConfig = require('../config/upload')
const photoProfileController = require('../controller/PhotoProfile')
const uploadPhoto = multer({storage: uploadConfig.storagePhoto})
const { verifyJWT } = require('../config/jwtFunctions')

router.post('/photo', verifyJWT, uploadPhoto.single('photo'), photoProfileController.savePhoto)
router.delete('/photo', verifyJWT, photoProfileController.deletePhoto)

module.exports = router