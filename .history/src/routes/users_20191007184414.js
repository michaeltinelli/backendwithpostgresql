const express = require('express')
const router = express.Router()
const multer = require('multer')
const uploadConfig = require('../config/upload')


router.get('/users', userController.getUsers)
router.post('/users', userController.addUser)
router.get('/users/:id', userController.getUser)
router.delete('/users/:id', userController.deleteUser)
