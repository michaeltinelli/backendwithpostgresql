
const express = require('express')
const router = express.Router()
const routerUsers = require('./src/routes/users')
const routerPosts = require('./src/routes/posts')
const routerPhotoProfile = require('./src/routes/photoProfile')



module.exports = {
    router,
    routerUsers,
    routerPosts,
    routerPhotoProfile
}