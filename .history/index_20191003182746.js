const express = require('express')
const app = express()
const port = 3000
const cors = require('cors')
const bodyParser = require('body-parser')
const routes = require('./routes')

app.use(cors())

//habilitar para entrar arqs como img,css,js
app.use(express.static('public'))
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
app.use(express.json())

app.use(routes)
app.listen(port, () => {
    console.log(`Backend is running on port ${port}`)
})