
const routerUsers = require('./src/routes/users')
const routerPosts = require('./src/routes/posts')
const routerPhotoProfile = require('./src/routes/photoProfile')
const routerImagePost = require('./src/routes/imagePost')


module.exports = {
    
    routerUsers,
    routerPosts,
    routerPhotoProfile,
    routerImagePost,
}