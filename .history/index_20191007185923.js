const express = require('express')
const app = express()
const port = 3000
const cors = require('cors')
const bodyParser = require('body-parser')
const {  routerUsers, routerPosts, routerPhotoProfile } = require('./routes')


app.use(cors())
app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.json())
app.use([  routerUsers, routerPosts, routerPhotoProfile, ])

app.listen(port, () => {
    console.log(`Backend is running on port ${port}`)
})