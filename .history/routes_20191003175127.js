const userController = require('./src/controller/Users')

module.exports = router  = {
    router.get('/users', userController.getUsers)
}