const userController = require('./src/controller/Users')
const express = require('express')
const router = express.Router()

router.get('/users', userController.getUsers)
router.post('/users', userController.addUser)
router.get('/users/:id', userController.getUser)
router.delete('/users/:id', userController.deleteUser)


module.exports = router