const userController = require('./src/controller/Users')
const postController = require('./src/controller/Posts')
const photoProfileController = require('./src/controller/PhotoProfile')

const express = require('express')
const router = express.Router()
const multer = require('multer')
const uploadConfig = require('./src/config/upload')

const routerUsers = require('./src/routes/users')
const routerPosts = require('./src/routes/posts')

const uploadPhoto = multer({storage: uploadConfig.storagePhoto})


router.post('/photo', uploadPhoto.single('photo'), photoProfileController.savePhoto)
router.delete('/photo', photoProfileController.deletePhoto)


module.exports = {
    router,
    routerUsers,
    routerPosts,
}