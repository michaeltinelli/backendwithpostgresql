const userController = require('./src/controller/Users')
const router = express.Router()

router.get('/users', userController.getUsers)