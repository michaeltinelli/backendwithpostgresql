const userController = require('./src/controller/Users')
const postController = require('./src/controller/Posts')
const express = require('express')
const router = express.Router()
const multer = require('multer')
const uploadConfig = require('./src/config/upload')
const { storage } = require('./src/config/upload')

//const upload = multer(uploadConfig)

router.get('/users', userController.getUsers)
router.post('/users', userController.addUser)
router.get('/users/:id', userController.getUser)
router.delete('/users/:id', userController.deleteUser)

router.get('/posts', postController.getPostsById)
router.post('/fulano', multer({ storage }))



module.exports = router