const userController = require('./src/controller/Users')
const postController = require('./src/controller/Posts')
const express = require('express')
const router = express.Router()

router.get('/users', userController.getUsers)
router.post('/users', userController.addUser)
router.get('/users/:id', userController.getUser)
router.delete('/users/:id', userController.deleteUser)

router.get('/posts', postController.getPostsById)
router.post('/fulano', console.log('aqui'), null)

module.exports = router