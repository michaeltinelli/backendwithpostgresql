// Update with your config settings.

module.exports = {

  staging: {
    client: 'postgresql',
    connection: {
      database: 'nodejs',
      user:     'postgres',
      password: 'danone11'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

};
