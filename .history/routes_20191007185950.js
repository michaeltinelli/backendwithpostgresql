
const routerUsers = require('./src/routes/users')
const routerPosts = require('./src/routes/posts')
const routerPhotoProfile = require('./src/routes/photoProfile')



module.exports = {
    
    routerUsers,
    routerPosts,
    routerPhotoProfile
}