const express = require('express')
const app = express()
const port = 3000
const cors = require('cors')

const router = express.Router()
const routes = require('./routes')
routes.router = router

//multipart - forms de arquivos como imagem

const bodyParser = require('body-parser')
const multer = require('multer')

app.use(cors())

//habilitar para entrar arqs como img,css,js
app.use(express.static('public'))
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.listen(port, () => {
    console.log(`Backend is running on port ${port}`)
})